package main

import (
	"bufio"
	"reflect"
	"strings"
	"testing"
)

func Test_readingFromStdin(t *testing.T) {
	input := "test1\ntest2\n\nfin"
	scantest := bufio.NewScanner(strings.NewReader(input))
	want := []string{"test1", "test2", "", "fin"}
	got := readingFromStdin(scantest)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("readingFromStdin() = %v, want %v", got, want)
	}
}

func Test_parseURL(t *testing.T) {
	tests := []struct {
		name string
		args []string
		want []string
	}{
		{"simples urls",
			[]string{"bla bla", "bla https://oneurl.com"},
			[]string{"https://oneurl.com"}},
		{"html samples",
			[]string{`<a class="button is-fullwidth is-outlined" href="https://gitlab.com/users/franckf/projects" target="_blank">gitlab</a>`, `<ul>		<li><a href="https://www.cert.ssi.gouv.fr" target="_blank">cert-fr</a></li>		</ul>`, `<li><a href="https://play.golang.org/" target="_blank">goplay</a></li>`},
			[]string{"https://gitlab.com/users/franckf/projects", "https://www.cert.ssi.gouv.fr", "https://play.golang.org/"}},
		{"multi url in one line",
			[]string{"bla bla", "bla https://oneurl.com", "bla https://url2.fr https://url3.net eol", "bla fin"},
			[]string{"https://oneurl.com", "https://url2.fr", "https://url3.net"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseURL(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseURL() %v = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
