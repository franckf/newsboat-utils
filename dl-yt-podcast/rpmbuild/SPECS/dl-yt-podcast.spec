Name:           dl-yt-podcast
Version:        main
Release:        1%{?dist}
Summary:        A golang clone of urlview

License:        GPLv3+
URL:            https://gitlab.com/franckf/%{name}
Source0:        https://gitlab.com/franckf/%{name}/-/archive/%{version}/%{name}-main.tar.gz

BuildRequires:  golang
Requires:       yt-dlp
BuildArch:      noarch

%description
A golang clone of urlview. Use it with with newboat to dl videos and podcasts

%prep
%autosetup

%build
%{make_build}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
%{make_build}
sudo cp %{name} %{_bindir}

%files
%license LICENSE
%doc README.md

%changelog
* Fri Dec 17 2021 franckf
- main version
