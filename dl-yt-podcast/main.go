package main

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var Version = "development"

func main() {
	if len(os.Args) > 1 {
		if os.Args[1] == "--version" {
			fmt.Println("Version:\t", Version)
			return
		}
	}

	scanner := bufio.NewScanner(os.Stdin)
	inputs := readingFromStdin(scanner)
	urls := parseURL(inputs)
	selectUrlAuto(urls)
}

// readingFromStdin get infos from buffer (stdin) and return a slice of string (a line by index)
func readingFromStdin(scanner *bufio.Scanner) []string {
	inputs := []string{}
	for scanner.Scan() {
		inputs = append(inputs, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Println("error while reading stdin:", err)
	}
	return inputs
}

// parseURL with regex
func parseURL(inputs []string) []string {
	urls := []string{}
	urlsRegex := regexp.MustCompile(`(((http|https|ftp|gopher)|mailto):(//)?[^ <>\"\t]*|(www|ftp)[0-9]?\\.[-a-z0-9.]+)[^ .,;\t\n\r<\">\\):]?[^, <>\"\t]*[^ .,;\t\n\r<\">\\):]`)
	for _, input := range inputs {
		extract := urlsRegex.FindAllString(input, -1)
		for _, url := range extract {
			urls = append(urls, strings.TrimSpace(url))
		}
	}
	return urls
}

// selectUrlAuto download files in differents ways according to source
func selectUrlAuto(urls []string) {
	if strings.Contains(urls[0], "youtube.com/watch") || strings.Contains(urls[0], "clips.twitch.tv") {
		downloadWithYTdl(urls[0])
	}
	podcastRegex := regexp.MustCompile(`.mp3|.mp4|.m4a`)
	for _, url := range urls {
		if strings.Contains(url, "storyboards") {
			openInFeh(url)
		}
		if podcastRegex.MatchString(url) {
			downloadPodcast(url)
			return
		}
	}
}

func openInFeh(url string) {
	fehOpen := "feh " + url
	output, err := exec.Command("bash", "-c", fehOpen).CombinedOutput()
	fmt.Println(string(output))
	if err != nil {
		fmt.Println("feh had an error:", err)
	}
}

func downloadWithYTdl(url string) {
	home := os.Getenv("HOME")
	youtubeDL := "yt-dlp --add-metadata -ic " + url
	os.Chdir(home + "/Vidéos/")
	fmt.Println("the download of", url, "has started")
	output, err := exec.Command("bash", "-c", youtubeDL).CombinedOutput()
	fmt.Println(string(output))
	if err != nil {
		fmt.Println("youtube-dl had an error:", err)
	}
	fmt.Println("the download of", url, "is done")
	os.Chdir(home)
}

func downloadPodcast(url string) {
	parseUrl := strings.Split(url, "/")
	filename := parseUrl[len(parseUrl)-1]
	home := os.Getenv("HOME")
	os.Chdir(home + "/Musique/")
	fmt.Println("the download of", filename, "has started")
	writefile, err := os.Create(filename)
	if err != nil {
		fmt.Println("the file creation had an error:", err)
	}
	defer writefile.Close()
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("the download of the podcast had an error", err)
	}
	defer resp.Body.Close()
	io.Copy(writefile, resp.Body)
	fmt.Println("the download of", filename, "is done")
	os.Chdir(home)
}
