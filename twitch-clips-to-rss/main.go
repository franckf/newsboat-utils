package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"text/template"
	"time"

	"twitch-clips-to-rss/env"
)

//curl 'https://gql.twitch.tv/gql' -H 'Client-Id: aaaaaaaaaa' --data-raw '[{"operationName":"ClipsCards__User","variables":{"login":"angledroit","limit":20,"criteria":{"filter":"LAST_MONTH"}},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"bbbbhbb"}}}]'

const graphql string = "https://gql.twitch.tv/gql"

var streamers = []string{"angledroit", "grafikart", "ultia", "rivenzi", "ponce", "mynthos", "theprimeagen", "alphacast", "joueur_du_grenier", "bagherajones", "hortyunderscore", "avamind", "antoinedaniel", "zerator", "mistermv"}

type reqgql struct {
	OperationName string          `json:"operationName"`
	Variables     reqgqlVariable  `json:"variables"`
	Extensions    reqgqlExtension `json:"extensions"`
}
type reqgqlVariable struct {
	Login    string         `json:"login"`
	Limit    int            `json:"limit"`
	Criteria reqgqlCriteria `json:"criteria"`
}
type reqgqlCriteria struct {
	Filter string `json:"filter"`
}
type reqgqlExtension struct {
	PersistedQuery reqgqlPersistedQuery `json:"persistedQuery"`
}
type reqgqlPersistedQuery struct {
	Version    int    `json:"version"`
	Sha256Hash string `json:"sha256Hash"`
}

type resgql struct {
	Data resData `json:"data"`
}
type resData struct {
	User resUser `json:"user"`
}
type resUser struct {
	Clips resClips `json:"clips"`
}
type resClips struct {
	Edges []resEdge `json:"edges"`
}
type resEdge struct {
	Node resNode `json:"node"`
}
type resNode struct {
	Title string `json:"title"`
	Url   string `json:"url"`
}

type clip struct {
	Streamer string
	Title    string
	Link     string
}

type rss struct {
	Version     string    `xml:"version,attr"`
	Title       string    `xml:"channel>title"`
	Link        string    `xml:"channel>link"`
	Description string    `xml:"channel>description"`
	Item        []rssItem `xml:"channel>item"`
}

type rssItem struct {
	Title string `xml:"title"`
	Link  string `xml:"link"`
}

func disable() bool {
	var home = os.Getenv("HOME")
	_, err := os.Stat(home + "/twitch-clips.sh")
	if err != nil {
		return false
	}
	return true
}

func main() {
	if time.Now().Weekday() != 0 {
		emptyRssFeed()
		return
	}
	if disable() {
		fmt.Println("disable")
		return
	}
	allClips := []clip{}
	for _, streamer := range streamers {
		current := reqgql{
			OperationName: "ClipsCards__User",
			Variables: reqgqlVariable{
				Login:    streamer,
				Limit:    20,
				Criteria: reqgqlCriteria{Filter: "LAST_WEEK"}},
			Extensions: reqgqlExtension{
				PersistedQuery: reqgqlPersistedQuery{
					Version:    1,
					Sha256Hash: env.Sha,
				},
			},
		}
		resCurrent := requestTwitch(current)
		clipsCurrent := parseTwitchClips(streamer, resCurrent)
		allClips = append(allClips, clipsCurrent...)
	}
	generateBashToDLClips(allClips)
}

func generateBashToDLClips(allClips []clip) {
	const script = `
#! /bin/sh

mkdir -p ~/Vidéos/clips
cd ~/Vidéos/clips

{{range .}}
echo "{{.Streamer}} - {{.Title}}"
youtube-dl {{.Link}}
{{end}}

echo "end of download"
`

	scriptGenerated := template.Must(template.New("").Parse(script))
	var scriptBuffer bytes.Buffer
	err := scriptGenerated.Execute(&scriptBuffer, allClips)
	if err != nil {
		fmt.Println("error during templating", err)
	}

	file, err := os.Create("twitch-clips.sh")
	if err != nil {
		fmt.Println("error during creating file", err)
	}
	defer file.Close()
	file.WriteString(scriptBuffer.String())

	err = os.Chmod("twitch-clips.sh", 0744)
	if err != nil {
		fmt.Println("error during permissions file", err)
	}
	home := os.Getenv("HOME")
	err = os.Rename("twitch-clips.sh", home+"/twitch-clips.sh")
	if err != nil {
		fmt.Println("error during moving file", err)
	}
}

func emptyRssFeed() {
	emptyRssFeed := rss{
		Version:     "2.0",
		Title:       "twitch clips only on sunday",
		Link:        "https://www.twitch.tv/",
		Description: "twitch clips only on sunday",
		Item: []rssItem{
			rssItem{
				Title: "empty",
				Link:  "https://www.twitch.tv/",
			},
		},
	}
	data, _ := xml.MarshalIndent(emptyRssFeed, "", "    ")
	fmt.Println(string(data))
}

func parseTwitchClips(streamer string, jsonRes []byte) (clips []clip) {
	var data []resgql
	err := json.Unmarshal(jsonRes, &data)
	if err != nil {
		fmt.Println("unmarshalling", err)
	}
	for _, v := range data[0].Data.User.Clips.Edges {
		var newClip clip
		newClip.Streamer = streamer
		newClip.Title = strings.Replace(v.Node.Title, `"`, ``, 2)
		newClip.Link = v.Node.Url
		clips = append(clips, newClip)
	}
	return
}

func requestTwitch(body reqgql) (response []byte) {
	var requestSlice []reqgql
	requestSlice = append(requestSlice, body)
	jsonReq, err := json.Marshal(requestSlice)
	if err != nil {
		fmt.Println("marshalling", err)
	}
	req, err := http.NewRequest(http.MethodPost, graphql, bytes.NewBuffer(jsonReq))
	if err != nil {
		fmt.Println("send request", err)
	}
	req.Header.Set("Client-Id", env.Clientid)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("send request", err)
	}
	defer resp.Body.Close()
	response, _ = io.ReadAll(resp.Body)
	return
}
