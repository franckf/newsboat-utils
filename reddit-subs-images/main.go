package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

type Atom struct {
	XMLName   xml.Name `xml:"http://www.w3.org/2005/Atom feed"`
	Title     string   `xml:"title"`
	Subtitle  string   `xml:"subtitle"`
	Id        string   `xml:"id"`
	Updated   string   `xml:"updated"`
	Rights    string   `xml:"rights"`
	Link      Link     `xml:"link"`
	Author    Author   `xml:"author"`
	EntryList []Entry  `xml:"entry"`
}

type Link struct {
	Href string `xml:"href,attr"`
}

type Author struct {
	Name  string `xml:"name"`
	Email string `xml:"email"`
}

type Entry struct {
	Title   string `xml:"title"`
	Summary string `xml:"summary"`
	Content string `xml:"content"`
	Id      string `xml:"id"`
	Updated string `xml:"updated"`
	Link    Link   `xml:"link"`
	Author  Author `xml:"author"`
}

func getContentFieldFromRss(redditSubRss string) (allContents []string) {
	var rssUrl = "https://www.reddit.com/r/" + redditSubRss + "/.rss"
	var rawRss = wget(rssUrl)
	var formatedRss Atom

	xml.Unmarshal([]byte(rawRss), &formatedRss)
	for i := 0; i < len(formatedRss.EntryList); i++ {
		allContents = append(allContents, formatedRss.EntryList[i].Content)
	}
	return
}

func wget(url string) (rssFeed string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Printf("err: %#+v\n", err)
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.80 Safari/537.36")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("err: %#+v\n", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("err: %#+v\n", err)
	}
	rssFeed = string(body)
	return
}

func getImagesFromContents(allContents []string) (allImages []string) {
	for _, content := range allContents {
		var pieces = strings.Split(content, `"`)
		var images = parseImages(pieces)
		// not great - to get only one image by content
		// use preview if nothing felt
		if len(images) == 2 {
			allImages = append(allImages, images[1])
		} else if len(images) == 1 {
			allImages = append(allImages, images[0])
		}
	}
	return
}

func parseImages(pieces []string) (images []string) {
	for _, piece := range pieces {
		var gif = strings.Contains(piece, ".gif")
		var jpg = strings.Contains(piece, ".jpg")
		var jpeg = strings.Contains(piece, ".jpeg")
		var png = strings.Contains(piece, ".png")
		if gif || jpg || jpeg || png {
			var fixAmp = strings.ReplaceAll(piece, "amp;", "")
			images = append(images, fixAmp)
		}
	}
	return
}

func downloadImages(subFolder string, imagesSelected []string) {
	os.Chdir(subFolder)
	for _, image := range imagesSelected {
		parseUrl := strings.Split(image, "/")
		filename := parseUrl[len(parseUrl)-1]
		writefile, err := os.Create(filename)
		if err != nil {
			fmt.Printf("err: %#+v\n", err)
		}
		defer writefile.Close()
		resp, err := http.Get(image)
		if err != nil {
			fmt.Printf("err: %#+v\n", err)
		}
		defer resp.Body.Close()
		io.Copy(writefile, resp.Body)
	}
	return
}

func disable() bool {
	var home = os.Getenv("HOME")
	_, err := os.Stat(home + "/noreddit")
	if err != nil {
		return false
	}
	return true
}

func main() {
	if disable() {
		fmt.Println("disable")
		return
	}
	for _, sub := range subs {
		fmt.Println("Start", sub)
		var allContents = getContentFieldFromRss(sub)
		var imagesSelected = getImagesFromContents(allContents)
		var home = os.Getenv("HOME")
		var subFolder = home + "/Images/" + sub
		os.Mkdir(subFolder, 0755)
		downloadImages(subFolder, imagesSelected)
	}
}
