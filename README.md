[![pipeline status](https://gitlab.com/franckf/newsboat-utils/badges/main/pipeline.svg)](https://gitlab.com/franckf/newsboat-utils/-/commits/main)
[![coverage report](https://gitlab.com/franckf/newsboat-utils/badges/main/coverage.svg)](https://gitlab.com/franckf/newsboat-utils/-/commits/main)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/franckf/newsboat-utils)](https://goreportcard.com/report/gitlab.com/franckf/newsboat-utils)

tools to improve my newsboat: downloaders, site2rss converters, ...

## CI

Gitlab CI run the tests, to be sure that it is working with the latest version of Go.  
To see the result of the coverage of `go test`, add the regular expression under _Settings > CI/CD > General pipelines > Test coverage parsing_  
`coverage: \d+.\d+% of statements`
