package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"
)

const scanunion string = "https://scantrad-union.com/"
const animultim string = "http://www.anime-ultime.net/index-0-1"

type rss struct {
	Version     string    `xml:"version,attr"`
	Title       string    `xml:"channel>title"`
	Link        string    `xml:"channel>link"`
	Description string    `xml:"channel>description"`
	Item        []rssItem `xml:"channel>item"`
}

type rssItem struct {
	Title string `xml:"title"`
	Link  string `xml:"link"`
}

func main() {
	scanunionHTML := wget(scanunion)
	matchSU := grep(scanunionHTML, "numerochapitre")
	rssSU := parseScanUnion(matchSU)

	animeHTML := wget(animultim)
	matchAU := grep(animeHTML, `align="left"`)
	rssAU := parseAnimUltim(matchAU)

	var combinedRSS []rssItem
	combinedRSS = append(combinedRSS, rssSU...)
	combinedRSS = append(combinedRSS, rssAU...)
	rssFeed := rss{
		Version:     "2.0",
		Title:       "scanunion and animeultime",
		Link:        scanunion,
		Description: "autogen rss from sites",
		Item:        combinedRSS,
	}
	data, _ := xml.MarshalIndent(rssFeed, "", "    ")
	fmt.Println(string(data))
}

// parseAnimeUltim doesn't handle number of episode - wait and see if it is a issue
func parseAnimUltim(matchs []string) (itemsAU []rssItem) {
	for _, match := range matchs {
		link := strings.Split(match, `"`)[5]
		anime := strings.Split(link, "/")[2]
		animultimlink := strings.Trim(animultim, "index-0-1")
		var currentItem rssItem
		currentItem.Title = anime
		currentItem.Link = animultimlink + link
		itemsAU = append(itemsAU, currentItem)
	}
	return
}

func parseScanUnion(matchs []string) (itemsSU []rssItem) {
	for _, match := range matchs {
		link := strings.Split(match, `"`)[1]
		manga := strings.Split(link, "/")[4]
		chapter := strings.Split(link, "/")[5]
		numero := strings.Trim(strings.Trim(chapter, ".00"), "chapter-")
		var currentItem rssItem
		currentItem.Title = manga + " " + chapter
		currentItem.Link = scanunion + "manga/" + manga + "/" + numero
		itemsSU = append(itemsSU, currentItem)
	}
	return
}

func wget(url string) (html []string) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("err: %#+v\n", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("err: %#+v\n", err)
	}
	html = strings.Split(string(body), "\n")
	return
}

func grep(lines []string, pattern string) (matchs []string) {
	regex, _ := regexp.Compile(pattern)
	for _, line := range lines {
		if regex.MatchString(line) {
			matchs = append(matchs, line)
		}
	}
	return
}
