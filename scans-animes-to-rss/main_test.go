package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_wget(t *testing.T) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, `
		<a href="http://zero-airisu.com/" target="_blank"><img class="lazy" height="26" width="74" data-href="http://img0.anime-ultime.net/images/img38041.png"title="Iris-Z&#039; Fansub" /></a>
		<a href="http://ida-fansub.blogspot.fr/" target="_blank"><img class="lazy" height="26" width="74" data-href="http://img1.anime-ultime.net/images/img48256.gif"title="Ida Fansub" /></a>
		<a href="http://sanu-fansub.org/accueil.html" target="_blank"><img class="lazy" height="26" width="74" data-href="http://img1.anime-ultime.net/images/img31516.jpg"title="Sanu Fansub" /></a>
		`)
	}

	req := httptest.NewRequest("GET", "http://www.anime-ultime.net/index-0-1", nil)
	w := httptest.NewRecorder()
	handler(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(resp.StatusCode)
	fmt.Println(resp.Header.Get("Content-Type"))
	fmt.Println(string(body))
}
